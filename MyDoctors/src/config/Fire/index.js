import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyB0UenWxMdj4xwR_YFvF1X5kg-Q83ssJXU',
  authDomain: 'my-doctors-02.firebaseapp.com',
  projectId: 'my-doctors-02',
  storageBucket: 'my-doctors-02.appspot.com',
  messagingSenderId: '993928116832',
  appId: '1:993928116832:web:7140c794f972bcb83de105',
});

const Fire = firebase;

export default Fire;
