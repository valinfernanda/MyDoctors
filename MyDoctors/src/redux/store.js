import {createStore} from 'redux';

const initialState = {
  loading: false,
  name: 'Valin Fernanda',
  address: 'Karasak',
};
const reducer = (state = initialState, action) => {
  if (action.type === 'SET_LOADING') {
    return {
      ...state,
      loading: action.value,
    };
  }
  if (action.type === 'SET_NAME') {
    return {
      ...state,
      name: 'Evita Lawrencia',
    };
  }
  return state;
};

const store = createStore(reducer);

export default store;
