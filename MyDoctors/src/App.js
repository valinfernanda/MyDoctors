import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import FlashMessage from 'react-native-flash-message';
import {Loading} from './components';
import {Provider, useSelector} from 'react-redux';
import store from './redux/store';
import {LogBox} from 'react-native';

const MainApp = () => {
  // const [loading, setLoading] = useState(false);
  //yang diatas jadinya ga dipake lagi karna udh pake stateGlobal
  const stateGlobal = useSelector((state) => state);
  console.log('state global', stateGlobal);
  //abis console log liat ke log apakah store kita udh kebaca di log
  LogBox.ignoreAllLogs(['Setting a Timer']);
  return (
    <>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
      {stateGlobal.loading && <Loading />}
    </>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
};

export default App;
